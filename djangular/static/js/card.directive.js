(function () {
    'use strict';

    angular.module('scrumboard.demo')
        .directive('scrumboardCard', CardDirective);

    function CardDirective() {
        return {
            templateUrl: '/static/html/card.html',
            restrict: 'E',
            controller: ['$scope', '$http', ($scope, $http) => {
                const url = 'scrumboard/cards/' + $scope.card.id + '/';
                const orig = { title: $scope.card.title, description: $scope.card.description };
                $scope.destList = $scope.list;


                function putCard() {
                    return $http.put(url, $scope.card);
                }

                function removeCardFromList(card, list) {
                    const cards = list.cards;
                    cards.splice(cards.indexOf(card), 1);
                }

                $scope.update = () => {
                    putCard()
                        .then(() => {
                            orig.description = $scope.card.description;
                            orig.title = $scope.card.title;
                        },
                            () => {
                                $scope.edit = false;
                                alert('Could not edit card');
                                $scope.card.description = orig.description;
                                $scope.card.title = orig.title;
                            });

                };
                $scope.modelOptions = {
                    debounce: 500
                };
                $scope.delete = () => {
                    $http.delete(url)
                        .then(
                            removeCardFromList($scope.card, $scope.list),
                            () => { alert('Could not delete') })
                }

                $scope.move = () => {
                    if ($scope.destList === undefined) {
                        return
                    }
                    $scope.card.clist = $scope.destList.id;
                    putCard().then(() => {
                        removeCardFromList($scope.card, $scope.list);
                        $scope.destList.cards.push($scope.card);
                    })
                }
            }]
        };
    }

})();