(() => {
    'use strict';

    angular
        .module('scrumboard.demo')
        .controller('LoginController',
            ['$scope', '$location', 'Login', LoginController]);

    function LoginController($scope, $location, Login) {
        $scope.login = () => {
            Login.login($scope.user)
                .then(() => { $location.url('/'); },
                    () => { $scope.login_error = "Invalid Username or Password" });
        }


        if (Login.isLoggedIn()) {
            $location.url('/');
        }
    }
})();