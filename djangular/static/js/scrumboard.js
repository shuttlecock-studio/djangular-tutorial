(function () {
    'use strict';

    angular.module('scrumboard.demo', ['ngRoute'])
        .controller('ScrumboardController', ['$scope', '$http','Login', ScrumboardController])


    function ScrumboardController($scope, $http,Login) {
        $scope.add = function (list, title) {
            var card = {
                clist: list.id,
                title: title

            };
            $http.post('/scrumboard/cards/', card)
                .then((response) => { list.cards.push(response.data) },
                    () => { alert('Could not create card') });

        };

        Login.redirectIfNotLoggedIn();
        $scope.logout = Login.logout;
        $scope.data = [];
        $scope.reverse = true;
        $scope.sortBy = 'title';
        $scope.showFilters = false;

        $http.get('/scrumboard/lists/')
            .then(function (response) {
                $scope.data = response.data;
            });
    }


}());